macro [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Pipeline status](https://gitlab.com/opennota/macro/badges/master/pipeline.svg)](https://gitlab.com/opennota/macro/commits/master)
=====

Expanding macros in Go using go/ast.

## Installation

    go get gitlab.com/opennota/macro

## Usage

    macro input.go.tmpl output.go
